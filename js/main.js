class Establecimiento_comercial{

    constructor(nit, ubicacion, direccion, cantidad_empleados, monto_dinero)
    {
    	this.nit = nit
        this.cantidad_empleados = cantidad_empleados
        this.ubicacion = ubicacion
        this.monto_dinero = monto_dinero
        this.direccion = direccion
    }
    getnit()
    {
    	return this.nit
    }
    setnit(nit)
    {
        this.nit = nit
    }
    getubicacion()
    {
        return this.ubicacion
    }
    setubicacion(ubicacion)
    {
        this.ubicacion = ubicacion
    }
    getcantidad_empleados()
    {
        return this.cantidad_empleados
    }
    setcantidad_empleados(cantidad_empleados)
    {
        this.cantidad_empleados = cantidad_empleados
    }
    getmonto_dinero()
    {
        return this.monto_dinero
    }
    setmonto_dinero(monto_dinero)
    {
        this.monto_dinero = monto_dinero
    }
    Vender(valor_producto){
        this.monto_dinero = monto_dinero + valor_producto
        return this.monto_dinero
    }
    Comprar(valor_producto){
        this.monto_dinero = monto_dinero - valor_producto
        return this.monto_dinero;
    }
}
class Persona{

    constructor(dni, nombre, monto_dinero)
    {
        this.dni = dni
        this.nombre = nombre
        this.monto_dinero = monto_dinero
    }
    getdni()
    {
        return this.dni
    }
    setdni(dni)
    {
        this.dni = dni
    }
    getnombre()
    {
        return this.nombre
    }
    setnombre(nombre)
    {
        this.nombre = nombre
    }
    getmonto_dinero()
    {
        return this.monto_dinero
    }
    setmonto_dinero(monto_dinero)
    {
        this.monto_dinero = monto_dinero
    }
}

class Empleado extends Persona{

    constructor(dni, nombre, monto_dinero, fecha_de_ingreso, ubicacion)
    {
        super(dni, nombre, monto_dinero)
        this.fecha_de_ingreso = fecha_de_ingreso
        this.ubicacion = ubicacion
    }
    getfecha_de_ingreso()
    {
        return this.fecha_de_ingreso
    }
    setfecha_de_ingreso(fecha_de_ingreso)
    {
        this.fecha_de_ingreso = fecha_de_ingreso
    }
    getubicacion()
    {
        return this.ubicacion
    }
    setubicacion(ubicacion)
    {
        this.ubicacion = ubicacion
    }
    getmonto_dinero()
    {
        return this.monto_dinero
    }
    setmonto_dinero(monto_dinero)
    {
        this.monto_dinero = monto_dinero
    }
    getdni()
    {
        return this.dni
    }
    setdni(dni)
    {
        this.dni = dni
    }
    getnombre()
    {
        return this.nombre
    }
    setnombre(nombre)
    {
        this.nombre = nombre
    }
    Atender(cliente){

        if(cliente >= 1)
        {
            return "Hola";
        }
         
    }
    Cobrar(salario){
        return this.monto_dinero = this.monto_dinero + salario
    }
}

class Cliente extends Persona{

    constructor(dni, nombre, monto_dinero)
    {
        super(dni, nombre, monto_dinero)        
    }
    getmonto_dinero()
    {
        return this.monto_dinero
    }
    setmonto_dinero(monto_dinero)
    {
        this.monto_dinero = monto_dinero
    }
    Comprar(valor_producto)
    {        
        return this.monto_dinero = this.monto_dinero - valor_producto            
    }
}
//producto = recurso_pedido.enviar_pedido(this.ubicacion, this.direccion)
class Miscelanea extends Establecimiento_comercial{
    
	constructor(nit, ubicacion, direccion, cantidad_empleados, monto_dinero, cantidad_de_recursos, recurso)
	{
        super(nit, ubicacion, direccion, cantidad_empleados, monto_dinero)        
        this.cantidad_de_recursos = cantidad_de_recursos
        this.recurso = recurso
    }
    getrecurso()
    {
        return this.recurso;
    }
    setrecurso(recurso)
    {
        this.recurso = recurso;
    }
    getcantidad_de_recursos()
    {
        return this.cantidad_de_recursos;
    }
    setcantidad_de_recursos(cantidad_de_recursos)
    {
        this.cantidad_de_recursos = cantidad_de_recursos;
    }
    hacer_pedido(productos)
    {
        if(productos.length > 0)
        {
            return productos
            
            
        }
    }
    recibir_pedido(productos){
            
        if(productos.length > 0)
        {
            return console.log("Se recibió el pedido de "+ productos)            
        }
    }
}
class Proveedor_Miscelanea extends Establecimiento_comercial{
    
	constructor(nit, ubicacion, direccion, cantidad_empleados, monto_dinero, cantidad_de_recursos, recurso)
	{
        super(nit, ubicacion, direccion, cantidad_empleados, monto_dinero)        
        this.cantidad_de_recursos = cantidad_de_recursos
        this.recurso = recurso
    }
    //recursoMisc = Miscelanea.recurso//puede ser unico o una lista
    //ubicacionMisc = Miscelanea.ubicacion
    //direccionMisc = Miscelanea.direccion
    getrecurso()
    {
        return this.recurso;
    }
    setrecurso(recurso)
    {
        this.recurso = recurso;
    }
    getcantidad_de_reursos()
    {
        return this.cantidad_de_reursos;
    }
    setcantidad_de_reursos(cantidad_de_reursos)
    {
        this.cantidad_de_reursos = cantidad_de_reursos;
    }
    hacer_pedido()
    {
        if (this.recurso != null || this.recurso != "")
        {
            console.log("El pedido fué realizado satisfactoriamente")
            return true
        }
    }
    //recursoProv = this.hacer_pedido(this.recurso)
    fabricar_producto(){
        if(recursoProv)
        {
            console.log("El producto fué fabricado")
            return true
        }
        else{
            console.log("Hace falta materia prima para la fabricacion del producto")
            return false
        }
    }
    //recursoMisc = fabricar_producto(this.recursoProv)
    enviar_pedido(){
        if((this.recursoMisc) && (ubicacionMisc != null || ubicacionMisc != null)&& (direccionMisc != null || direccionMisc != null))
        {
            console.log("Los datos para enviar el pedido han sido recibidos, ha sido enviado")
            return true
        }
        else{
            console.log("A habido un problema con los datos del envío y/o con el producto, verifique los datos e intente mas tarde")
            return false
        }
    }
}

class Proveedor_Restaurante extends Establecimiento_comercial{

	constructor(nit, ubicacion, direccion, cantidad_empleados, monto_dinero, cantidad_de_recursos, recurso)
	{
        super(nit, ubicacion, direccion, cantidad_empleados, monto_dinero)        
        this.cantidad_de_recursos = cantidad_de_recursos
        this.recurso = recurso
    }
    //recursoRest = Restaurante.recurso//puede ser unico o una lista
    //ubicacionRest = Restaurante.ubicacion
    //direccionRest = Restaurante.direccion
    getrecurso()
    {
        return this.recurso;
    }
    setrecurso(recurso)
    {
        this.recurso = recurso;
    }
    getcantidad_de_reursos()
    {
        return this.cantidad_de_reursos;
    }
    setcantidad_de_reursos(cantidad_de_reursos)
    {
        this.cantidad_de_reursos = cantidad_de_reursos;
    }
    hacer_pedido()
    {
        if (this.recurso != null || this.recurso != "")
        {
            console.log("El pedido fué realizado satisfactoriamente")
            return true
        }
    }
    //recursoProv = this.hacer_pedido(this.recurso)
    fabricar_producto(){
        if(recursoProv)
        {
            console.log("El producto fué fabricado")
            return true
        }
        else{
            console.log("Hace falta materia prima para la fabricacion del producto")
            return false
        }
    }
    //recursoRest = fabricar_producto(this.recursoProv)
    enviar_pedido(){
        if((this.recursoRest) && (ubicacionRest != null || ubicacionRest != null)&& (direccionRest != null || direccionRest != null))
        {
            console.log("Los datos para enviar el pedido han sido recibidos, ha sido enviado")
            return true
        }
        else{
            console.log("A habido un problema con los datos del envío y/o con el producto, verifique los datos e intente mas tarde")
            return false
        }
    }
}

class Restaurante extends Establecimiento_comercial{
    
    constructor(nit, ubicacion, direccion, cantidad_empleados, monto_dinero, cantidad_de_recursos, recurso)
	{
        super(nit, ubicacion, direccion, cantidad_empleados, monto_dinero)        
        this.cantidad_de_recursos = cantidad_de_recursos
        this.recurso = recurso
    }
    //producto = recurso_pedido.enviar_pedido(this.ubicacion, this.direccion)
    getrecurso()
    {
        return this.recurso;
    }
    setrecurso(recurso)
    {
        this.recurso = recurso;
    }
    getcantidad_de_recursos()
    {
        return this.cantidad_de_recursos;
    }
    setcantidad_de_recursos(cantidad_de_recursos)
    {
        this.cantidad_de_recursos = cantidad_de_recursos;
    }
    hacer_pedido()
    {
        if(this.cantidad_de_recursos <= 50)
        {
            console.log("La cantidad es crítica, se ha realizado el pedido")
            return true
        }
        else
        {
            console.log("No se realizó el pedido, las cantidades son normales")
            return false
        }
    }
    recibir_pedido(){
        if(this.producto)
        {
            console.log("Se recibió el pedido")
            return true
        }
    }
}

class Proveedor_Licorera extends Establecimiento_comercial{

	constructor(nit, ubicacion, direccion, cantidad_empleados, monto_dinero, cantidad_de_recursos, recurso)
	{
        super(nit, ubicacion, direccion, cantidad_empleados, monto_dinero)        
        this.cantidad_de_recursos = cantidad_de_recursos
        this.recurso = recurso
    }
    //recursoLic = Licorera.recurso//puede ser unico o una lista
    //ubicacionLic = Licorera.ubicacion
    //direccionLic = Licorera.direccion
    getrecurso()
    {
        return this.recurso;
    }
    setrecurso(recurso)
    {
        this.recurso = recurso;
    }
    getcantidad_de_reursos()
    {
        return this.cantidad_de_reursos;
    }
    setcantidad_de_reursos(cantidad_de_reursos)
    {
        this.cantidad_de_reursos = cantidad_de_reursos;
    }
    hacer_pedido()
    {
        if (this.recurso != null || this.recurso != "")
        {
            console.log("El pedido fué realizado satisfactoriamente")
            return true
        }
    }
//    recursoProv = this.hacer_pedido(this.recurso)
    fabricar_producto(){
        if(recursoProv)
        {
            console.log("El producto fué fabricado")
            return true
        }
        else{
            console.log("Hace falta materia prima para la fabricacion del producto")
            return false
        }
    }
    //recursoLic = fabricar_producto(this.recursoProv)
    enviar_pedido(){
        if((this.recursoLic) && (ubicacionLic != null || ubicacionLic != null)&& (direccionLic != null || direccionLic != null))
        {
            console.log("Los datos para enviar el pedido han sido recibidos, ha sido enviado")
            return true
        }
        else{
            console.log("A habido un problema con los datos del envío y/o con el producto, verifique los datos e intente mas tarde")
            return false
        }
    }
 
}

class Licorera extends Establecimiento_comercial{
        
	constructor(nit, ubicacion, direccion, cantidad_empleados, monto_dinero, cantidad_de_recursos, recurso)
	{
        super(nit, ubicacion, direccion, cantidad_empleados, monto_dinero)        
        this.cantidad_de_recursos = cantidad_de_recursos
        this.recurso = recurso
    }
    //producto = recurso_pedido.enviar_pedido(this.ubicacion, this.direccion)
    getrecurso()
    {
        return this.recurso;
    }
    setrecurso(recurso)
    {
        this.recurso = recurso;
    }
    getcantidad_de_recursos()
    {
        return this.cantidad_de_recursos;
    }
    setcantidad_de_recursos(cantidad_de_recursos)
    {
        this.cantidad_de_recursos = cantidad_de_recursos;
    }
    hacer_pedido()
    {
        if(this.cantidad_de_recursos <= 50)
        {
            console.log("La cantidad es crítica, se ha realizado el pedido")
            return true
        }
        else
        {
            console.log("No se realizó el pedido, las cantidades son normales")
            return false
        }
    }
    recibir_pedido(){
        if(this.producto)
        {
            console.log("Se recibió el pedido")
            return true
        }
    }
}

class Proveedor_Panaderia extends Establecimiento_comercial{

	constructor(nit, ubicacion, direccion, cantidad_empleados, monto_dinero, cantidad_de_recursos, recurso)
	{
        super(nit, ubicacion, direccion, cantidad_empleados, monto_dinero)        
        this.cantidad_de_recursos = cantidad_de_recursos
        this.recurso = recurso
    }
    //recursoPana = Panaderia.recurso//puede ser unico o una lista
    //ubicacionPana = Panaderia.ubicacion
    //direccionPana = Panaderia.direccion
    getrecurso()
    {
        return this.recurso;
    }
    setrecurso(recurso)
    {
        this.recurso = recurso;
    }
    getcantidad_de_reursos()
    {
        return this.cantidad_de_reursos;
    }
    setcantidad_de_reursos(cantidad_de_reursos)
    {
        this.cantidad_de_reursos = cantidad_de_reursos;
    }
    hacer_pedido()
    {
        if (this.recurso != null || this.recurso != "")
        {
            console.log("El pedido fué realizado satisfactoriamente")
            return true
        }
    }
    //recursoProv = this.hacer_pedido(this.recurso)
    fabricar_producto(){
        if(recursoProv)
        {
            console.log("El producto fué fabricado")
            return true
        }
        else{
            console.log("Hace falta materia prima para la fabricacion del producto")
            return false
        }
    }
    //recursoPana = fabricar_producto(this.recursoProv)
    enviar_pedido(){
        if((this.recursoPana) && (ubicacionPana != null || ubicacionPana != null)&& (direccionPana != null || direccionPana != null))
        {
            console.log("Los datos para enviar el pedido han sido recibidos, ha sido enviado")
            return true
        }
        else{
            console.log("A habido un problema con los datos del envío y/o con el producto, verifique los datos e intente mas tarde")
            return false
        }
    }
 
}

class Panaderia extends Establecimiento_comercial{
    
    constructor(nit, ubicacion, direccion, cantidad_empleados, monto_dinero, cantidad_de_recursos, recurso)
	{
        super(nit, ubicacion, direccion, cantidad_empleados, monto_dinero)        
        this.cantidad_de_recursos = cantidad_de_recursos
        this.recurso = recurso
    }
    //producto = recurso_pedido.enviar_pedido(this.ubicacion, this.direccion)
    getrecurso()
    {
        return this.recurso;
    }
    setrecurso(recurso)
    {
        this.recurso = recurso;
    }
    getcantidad_de_recursos()
    {
        return this.cantidad_de_recursos;
    }
    setcantidad_de_recursos(cantidad_de_recursos)
    {
        this.cantidad_de_recursos = cantidad_de_recursos;
    }
    hacer_pedido()
    {
        if(this.cantidad_de_recursos <= 50)
        {
            console.log("La cantidad es crítica, se ha realizado el pedido")
            return true
        }
        else
        {
            console.log("No se realizó el pedido, las cantidades son normales")
            return false
        }
    }
    hacer_pan(){
        this.recurso = ("Harina", "Huevos", "Mantequilla", "Azucar", "Sal", "Levadura")
        if(this.recurso.lenght == 6)
        {
            console.log("El pan está en el horno")
            return true
        }
    }
    recibir_pedido(){
        if(this.producto)
        {
            console.log("Se recibió el pedido")
            return true
        }
    }
}

class Proveedor_Peluqueria extends Establecimiento_comercial{

	constructor(nit, ubicacion, direccion, cantidad_empleados, monto_dinero, cantidad_de_recursos, recurso)
	{
        super(nit, ubicacion, direccion, cantidad_empleados, monto_dinero)        
        this.cantidad_de_recursos = cantidad_de_recursos
        this.recurso = recurso
    }
    /*recursoPeluq = Peluqueria.recurso//puede ser unico o una lista
    ubicacionPeluq = Peluqueria.ubicacion
    direccionPeluq = Peluqueria.direccion*/
    getrecurso()
    {
        return this.recurso;
    }
    setrecurso(recurso)
    {
        this.recurso = recurso;
    }
    getcantidad_de_reursos()
    {
        return this.cantidad_de_reursos;
    }
    setcantidad_de_reursos(cantidad_de_reursos)
    {
        this.cantidad_de_reursos = cantidad_de_reursos;
    }
    hacer_pedido()
    {
        if (this.recurso != null || this.recurso != "")
        {
            console.log("El pedido fué realizado satisfactoriamente")
            return true
        }
    }
    //recursoProv = this.hacer_pedido(this.recurso)
    fabricar_producto(){
        if(recursoProv)
        {
            console.log("El producto fué fabricado")
            return true
        }
        else{
            console.log("Hace falta materia prima para la fabricacion del producto")
            return false
        }
    }
    //recursoPeluq = fabricar_producto(this.recursoProv)
    enviar_pedido(){
        if((this.recursoPeluq) && (ubicacionPeluq != null || ubicacionPeluq != null)&& (direccionPeluq != null || direccionPeluq != null))
        {
            console.log("Los datos para enviar el pedido han sido recibidos, ha sido enviado")
            return true
        }
        else{
            console.log("A habido un problema con los datos del envío y/o con el producto, verifique los datos e intente mas tarde")
            return false
        }
    }
}

class Peluqueria extends Establecimiento_comercial{
    
    constructor(nit, ubicacion, direccion, cantidad_empleados, monto_dinero, cantidad_de_recursos, recurso)
	{
        super(nit, ubicacion, direccion, cantidad_empleados, monto_dinero)        
        this.cantidad_de_recursos = cantidad_de_recursos
        this.recurso = recurso
    }
    //producto = recurso_pedido.enviar_pedido(this.ubicacion, this.direccion)
    getrecurso()
    {
        return this.recurso;
    }
    setrecurso(recurso)
    {
        this.recurso = recurso;
    }
    getcantidad_de_recursos()
    {
        return this.cantidad_de_recursos;
    }
    setcantidad_de_recursos(cantidad_de_recursos)
    {
        this.cantidad_de_recursos = cantidad_de_recursos;
    }
    hacer_pedido()
    {
        if(this.cantidad_de_recursos <= 50)
        {
            console.log("La cantidad es crítica, se ha realizado el pedido")
            return true
        }
        else
        {
            console.log("No se realizó el pedido, las cantidades son normales")
            return false
        }
    }
    recibir_pedido(){
        if(this.producto)
        {
            console.log("Se recibió el pedido")
            return true
        }
    }
}

class Proveedor_Placita_de_fruta extends Establecimiento_comercial{

	constructor(nit, ubicacion, direccion, cantidad_empleados, monto_dinero, cantidad_de_recursos, recurso)
	{
        super(nit, ubicacion, direccion, cantidad_empleados, monto_dinero)        
        this.cantidad_de_recursos = cantidad_de_recursos
        this.recurso = recurso
    }
    /*recursoPana = Placita_de_fruta.recurso//puede ser unico o una lista
    ubicacionPana = Placita_de_fruta.ubicacion
    direccionPana = Placita_de_fruta.direccion*/
    getrecurso()
    {
        return this.recurso;
    }
    setrecurso(recurso)
    {
        this.recurso = recurso;
    }
    getcantidad_de_reursos()
    {
        return this.cantidad_de_reursos;
    }
    setcantidad_de_reursos(cantidad_de_reursos)
    {
        this.cantidad_de_reursos = cantidad_de_reursos;
    }
    hacer_pedido()
    {
        if (this.recurso != null || this.recurso != "")
        {
            console.log("El pedido fué realizado satisfactoriamente")
            return true
        }
    }
    //recursoProv = this.hacer_pedido(this.recurso)
    fabricar_producto(){
        if(recursoProv)
        {
            console.log("El producto fué fabricado")
            return true
        }
        else{
            console.log("Hace falta materia prima para la fabricacion del producto")
            return false
        }
    }
    //recursoPana = fabricar_producto(this.recursoProv)
    enviar_pedido(){
        if((this.recursoPana) && (ubicacionPana != null || ubicacionPana != null)&& (direccionPana != null || direccionPana != null))
        {
            console.log("Los datos para enviar el pedido han sido recibidos, ha sido enviado")
            return true
        }
        else{
            console.log("A habido un problema con los datos del envío y/o con el producto, verifique los datos e intente mas tarde")
            return false
        }
    }
}

class Placita_de_fruta extends Establecimiento_comercial{
        
	constructor(nit, ubicacion, direccion, cantidad_empleados, monto_dinero, cantidad_de_recursos, recurso)
	{
        super(nit, ubicacion, direccion, cantidad_empleados, monto_dinero)        
        this.cantidad_de_recursos = cantidad_de_recursos
        this.recurso = recurso
    }
    //producto = recurso_pedido.enviar_pedido(this.ubicacion, this.direccion)
    getrecurso()
    {
        return this.recurso;
    }
    setrecurso(recurso)
    {
        this.recurso = recurso;
    }
    getcantidad_de_recursos()
    {
        return this.cantidad_de_recursos;
    }
    setcantidad_de_recursos(cantidad_de_recursos)
    {
        this.cantidad_de_recursos = cantidad_de_recursos;
    }
    hacer_pedido()
    {
        if(this.cantidad_de_recursos <= 50)
        {
            console.log("La cantidad es crítica, se ha realizado el pedido")
            return true
        }
        else
        {
            console.log("No se realizó el pedido, las cantidades son normales")
            return false
        }
    }
    recibir_pedido(){
        if(this.producto)
        {
            console.log("Se recibió el pedido")
            return true
        }
    }
}

class Venta{
    constructor(vendedor, productos, cantidad_productos, valor_productos)
    {
        this.vendedor = vendedor        
        this.productos = productos
        this.cantidad_productos = cantidad_productos
        this.valor_productos = valor_productos
        
    }
    getvendedor(){
        return this.vendedor
    }
    setvendedor(vendedor){
        this.vendedor = vendedor
    }     
    getproductos(){
        return this.productos
    }
    setproductos(productos){
        this.productos = productos
    }
    getcantidad_productos(){
        return this.cantidad_productos
    }
    setcantidad_productos(cantidad_productos){
        this.cantidad_productos = cantidad_productos
    }
    getvalor_productos(){
        return this.valor_productos
    }
    setvalor_productos(valor_productos){
        this.valor_productos = valor_productos
    }
    Calcular_totalvendido$(){
        let total_vendido$ = 0;
       //console.log("entro a la funcion del total")

        if(this.cantidad_productos.lenght > this.valor_productos.lenght || this.cantidad_productos.lenght < this.valor_productos.lenght)
        {
            console.log("la cantidad de datos no concuerda")
            return total_vendido$ = "undefined"
        }
        else{
            //console.log("entro a la condicion correcta")            
            for(let i = 0 ; i < this.cantidad_productos.length ; i++)
            {
               // console.log("estoy en la posicion "+i)
                total_vendido$ = total_vendido$ + (this.cantidad_productos[i] * this.valor_productos[i])
                
            }            
            return total_vendido$ 
        }
        
        
        //DUDA
    }
    Calcular_utilidad(total_vendido, costo_productos){
        let utilidad = 0
        let subtotal = 0
        for (let i = 0; i < costo_productos.length; i++) {
            subtotal = subtotal + costo_productos[i]            
        }
        utilidad = total_vendido - subtotal
        return utilidad 
    }   
}

class Producto{
    constructor(codigo, nombre, cantidad, costo, valor){
        this.codigo = codigo
        this.nombre = nombre 
        this.cantidad = cantidad
        this.costo = costo
        this.valor = valor        
    }
    getcodigo(){
        return this.codigo
    }
    setcodigo(codigo){
        this.codigo = codigo
    }
    getnombre(){
        return this.nombre
    }
    setnombre(nombre){
        this.nombre = nombre
    }
    getcantidad(){
        return this.cantidad
    }
    setcantidad(cantidad){
        this.cantidad = cantidad
    }
    getcosto(){
        return this.costo
    }
    setcosto(costo){
        if(costo < this.valor){
            this.costo = costo
            console.log("El costo es menor al valor")
        }
        else{
            console.log("El costo es mayor al valor")
            this.costo = costo
        }
        
    }
    getvalor(){        
        return this.valor
    }
    setvalor(valor){
        if(valor > this.costo)
        {
            this.valor = valor
            console.log("el valor es mayor al costo")
        }else{
            this.valor = valor
            console.log("el valor es menor al costo")
        }
        
    }
}
